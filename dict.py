#!/usr/bin/python

with open('dict.csv') as f:
  d = dict(x.rstrip().split(",") for x in f)

info = """
Enter desired word or statement below.
Words not found will return as entered.
Program does not currently support phrases.
"""

print(info)

statement = input("Enter word or statement: ")
statement = statement.lower()
word_list = statement.split(" ")

barbaric_list = [d.get(n, n) for n in word_list]
barbaric_string = " ".join(barbaric_list)

print(barbaric_string)
